Spree::ProductsController.class_eval do
  include Spree::ProductPathHelper
  
  before_filter :set_canonical, :only => [:show, :index]
  before_render :load_taxon, :only => :show

  private

  def set_canonical
    @canonical_url = "http://" + Spree::Config[:site_url] + product_path(@product)
    current_url = "#{request.protocol}#{request.host_with_port}#{request.path}"
    #redirect_to @canonical_url + (request.query_string ? "?#{request.query_string}" : ''), :status => :moved_permanently if @canonical_url != current_url
  end

  def load_taxon
    referer = request.env['HTTP_REFERER']
    if referer        
      begin
        referer_path = URI.parse(request.env['HTTP_REFERER']).path
        # Fix for #2249
      rescue URI::InvalidURIError
        # Do nothing
      else
        if referer_path && referer_path.match(/\/(.*)/)
          @taxon = Spree::Taxon.find_by_permalink($1)
        end
      end
    end
  end    
end