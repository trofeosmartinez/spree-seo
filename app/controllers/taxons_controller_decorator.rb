Spree::TaxonsController.class_eval do

  before_filter :set_canonical, :only => :show

  private

  def set_canonical
    @taxon = Spree::Taxon.find_by_permalink!(params[:id])
    return unless @taxon    
    page_param = (params[:page] ? "?page=#{params[:page]}" : '')
    per_page_param = (params[:per_page] ? ((params[:page] ? '&' : '?') + "per_page=#{params[:per_page]}") : '')
    @canonical_url = ("http://" + Spree::Config[:site_url] + nested_taxons_path(@taxon.permalink)).html_safe
    current_url = "#{request.protocol}#{request.host_with_port}#{request.path}" + page_param + per_page_param
    #redirect_to @canonical_url + (request.query_string ? "?#{request.query_string}" : ''), :status => :moved_permanently and return if @canonical_url != current_url
  end
  
end