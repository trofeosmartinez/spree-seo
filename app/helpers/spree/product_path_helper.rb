module Spree
  module ProductPathHelper
    def self.included(base)
      base.class_eval do
        def product_path(product)
          if product.try(:taxon_permalink)
            "/#{product.taxon_permalink.permalink}/#{product.to_param}"
          else
            products_path + "/#{product.to_param}"
          end
        end       
      end       
    end
  end
end